#include <windows.h>
#include <string.h>
#include <stdlib.h>

void* ptr = (void*)0x6A3F4D;

void write_memory(const char bytes[]) {
    DWORD old_prot;
    size_t size = strlen(bytes);

    VirtualProtect(ptr, size, PAGE_READWRITE, &old_prot);
    memcpy(ptr, bytes, size);
    VirtualProtect(ptr, size, old_prot, &old_prot);
}

int __stdcall DllMain(HMODULE module, DWORD reason, LPVOID reserved) {
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        write_memory("\xB0\x05\x90\x90\x90\x90");
        break;
    case DLL_PROCESS_DETACH:
        write_memory("\x8A\x86\x8A\x04\x00\x00");
        break;
    };
    return 1;
}
